/*
 * main implementation: use this 'C' sample to create your own application
 *
 */

#include "S32K144.h" /* include peripheral declarations S32K144 */
#include "s32k144_evb_led.h"

/* Port PTD0, bit 0: FRDM EVB output to blue LED */
#define PTD0 0
#define COUNTER_LIMIT 500000

void WDOG_disable (void){
	WDOG->CNT=0xD928C520;    /*Unlock watchdog*/
	WDOG->TOVAL=0x0000FFFF;  /*Maximum timeout value*/
	WDOG->CS = 0x00002100;   /*Disable watchdog*/
}

int main(void)
{
	int counter = 0;

	WDOG_disable();
	el_Enable_Red_LED();

	for(;;) {
		counter++;

		if(counter > COUNTER_LIMIT)
		{
			counter = 0;
			el_Toggle_Red_LED();
		}
	}

	return 0;
}
