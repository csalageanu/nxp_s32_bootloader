/*
 * main implementation: use this 'C' sample to create your own application
 *
 */


#include "S32K144.h" /* include peripheral declarations S32K144 */

/* Bootloader definitions */
/* Start address for the application received by the bootloader
 * application vector table should start at this address
 * */
#define APP_START_ADDRESS	0x1000

/* Port PTD0, bit 0: FRDM EVB output to blue LED */
#define PTD0 0
#define COUNTER_LIMIT 1000000


/* Global variables */
uint8_t boot_from_comm = 0;				/* Used to signal activity on the comm channel */


/* Prototype */
void JumpToUserApplication( unsigned int userSP,  unsigned int userStartup);
void WDOG_disable(void);


/* Main Application*/
int main(void)
{
	int counter = 0;
	WDOG_disable();

	el_Enable_Blue_LED();

	/* Initialize clock */
	clock_initi();

	/* Initialize communication interfaces */
	init_comm();

	/* Initialize timeout */
	init_timeout();

	do
	{
		uint8_t word_received = comm_status_rx();
		if(word_received)
		{
			boot_from_comm = 1;
			comm_download_app();
		}
	} while((!timeout()) & (!boot_from_comm));

	/* Disable all systems and leave device as if coming out of reset */
	disable_timeout();
	disable_comm();
	reset_clock();

	/* Check if a valid application is loaded and jump to it */
	JumpToUserApplication(*((uint32_t*)APP_START_ADDRESS), *((uint32_t*)(APP_START_ADDRESS + 4)));

    /* Should never return from application code */
	for(;;)
	{
		counter++;

		if(counter > COUNTER_LIMIT)
		{
			counter = 0;
			PTD->PTOR |= 1<<PTD0;
		}
	}

    /* Never leave main */
	return 0;
}
/**
 * Used to jump to the entry point of the user application
 * The Vector table of the user application must be located at 0x1000
 *
 * */
void JumpToUserApplication( unsigned int userSP,  unsigned int userStartup)
{
	/* Check if Entry address is erased and return if erased */
	if(userSP == 0xFFFFFFFF){
		return;
	}

	el_Turn_Off_Blue_LED();

	/* Set up stack pointer */
	__asm("msr msp, r0");
	__asm("msr psp, r0");

	/* Relocate vector table */
	S32_SCB->VTOR = (uint32_t)APP_START_ADDRESS;

	/* Jump to application PC (r1) */
	__asm("mov pc, r1");
}
/**
 *
 */
void WDOG_disable(void)
{
	WDOG->CNT=0xD928C520;    /*Unlock watchdog*/
	WDOG->TOVAL=0x0000FFFF;  /*Maximum timeout value*/
	WDOG->CS = 0x00002100;   /*Disable watchdog*/
}

////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
