/**
  ******************************************************************************
  * file    s32k144_evb_led.c
  * date    20-Jan-2019
  ******************************************************************************
  */

#include "S32K144.h" /* include peripheral declarations S32K144 */
#include "s32k144_evb_led.h"

/* Port PTD0: EVB output to Blue LED */
#define BLUE_LED_PIN 0
/* Port PTD15: EVB output to Red LED */
#define RED_LED_PIN 15

///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Function 
//******************************************************************************
void el_Enable_Blue_LED(void)
{
	PCC-> PCCn[PCC_PORTD_INDEX] |= PCC_PCCn_CGC_MASK; /* Enable clock to PORT D */
	PTD->PDDR |= 1<<BLUE_LED_PIN; /* Port D0: Data Direction= output */
	PORTD->PCR[0] |= 0x00000100; /* Port D0: MUX = GPIO */
}

//******************************************************************************
// Function 
//******************************************************************************
void el_Turn_Off_Blue_LED(void)
{
	PTD->PSOR |= 1<<BLUE_LED_PIN;
}

//******************************************************************************
// Function 
//******************************************************************************
void el_Toggle_Blue_LED(void)
{
	PTD->PTOR |= 1<<BLUE_LED_PIN;
}

//******************************************************************************
// Function 
//******************************************************************************
void el_Enable_Red_LED(void)
{
	PCC-> PCCn[PCC_PORTD_INDEX] |= PCC_PCCn_CGC_MASK; /* Enable clock to PORT D */
	PTD->PDDR |= 1<<RED_LED_PIN; /* Port D0: Data Direction= output */
	PORTD->PCR[RED_LED_PIN] |= 0x00000100; /* Port D0: MUX = GPIO */
}

//******************************************************************************
// Function 
//******************************************************************************
void el_Turn_Off_Red_LED(void)
{
	PTD->PSOR |= 1<<RED_LED_PIN;
}

//******************************************************************************
// Function 
//******************************************************************************
void el_Toggle_Red_LED(void)
{
	PTD->PTOR |= 1<<RED_LED_PIN;
}
