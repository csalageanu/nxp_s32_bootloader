/*
 * main implementation: use this 'C' sample to create your own application
 *
 */


#include "S32K144.h" /* include peripheral declarations S32K144 */
#define PTD16 16
#define COUNTER_LIMIT 200000

void WDOG_disable (void){
	WDOG->CNT=0xD928C520;    /*Unlock watchdog*/
	WDOG->TOVAL=0x0000FFFF;  /*Maximum timeout value*/
	WDOG->CS = 0x00002100;   /*Disable watchdog*/
}

int main(void)
{
	int counter = 0;

	WDOG_disable();

	PCC-> PCCn[PCC_PORTD_INDEX] = PCC_PCCn_CGC_MASK; /* Enable clock to PORT D */
	PTD->PDDR |= 1<<PTD16; /* Port D16: Data Direction= output */
	PORTD->PCR[PTD16] = 0x00000100; /* Port D0: MUX = GPIO */

	for(;;) {
		counter++;

		if(counter > COUNTER_LIMIT) {
			counter = 0;
			PTD->PTOR |= 1<<PTD16;
		}
	}


	return 0;
}
